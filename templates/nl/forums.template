<!--TITLE:[Mailinglijsten / Forums]-->

<h1 class="title">Belangrijke Mailinglijsten / Forums</h1>

<p>Wine heeft verschillende mailinglijsten en forums.
Voor de gebruiker staan hieronder de belangrijkste:</p>

<ul>
    <li>De <a href="//forum.winehq.org">WineHQ Forums</a></li>

    <li>Ubuntu gebruikers kunnen ook het <a href="http://ubuntuforums.org/forumdisplay.php?f=313">Wine Ubuntu Forum</a> bezoeken

    <li>Om op de hoogte gehouden te worden van nieuw versies kan worden ingeschreven worden op de <a href="//www.winehq.org/mailman/listinfo/wine-announce">Wine-Announce mailinglijst</a></li>
</ul>

<p>Maak alstublieft niet nog een ander Engelstalig Wine forum.
(Een Wine-hoekje in een forum van een populaire Linux variant kan nog)</p>

<p>Mocht u nog andere actieve Wine forums weten, vooral niet Engelstalige,
laat het ons dan weten. Dan kan die toegevoegd worden aan de lijst.
(En e-mail naar dank@kegel.com is voldoende)</p>

<h2>Alle WineHQ Mailinglijsten</h2>

<p>WineHQ heeft mailinglijsten om patches in te dienen, om Git-commits te
volgen en om te discussiëren over Wine. Er kan worden ingeschreven via de
<a href="https://www.winehq.org/mailman3/postorius/lists/?all-lists">website</a>.</p>

<p><b>Let op:</b> Voordat u naar een bericht naar maillijst stuurt, moet u ingeschreven zijn.
Anders behandeld de software van de mailinglijsten uw bericht als spam.</p>

<ul class="roomy">

  <li>
    <b><a href="mailto:wine-announce@winehq.org">wine-announce@winehq.org</a></b><br>
    [<a href="https://www.winehq.org/mailman3/postorius/lists/wine-announce.winehq.org/">In- uitschrijven</a>]
    [<a href="https://www.winehq.org/mailman3/hyperkitty/list/wine-announce@winehq.org/">Archief /a>]
    Een lijst (alleen-lezen) met weinig berichten (2/maand) voor aankondigingen van Wine versies
    en belangrijk nieuws over Wine of WineHQ.
  </li>

  <li>
    <b><a href="mailto:wine-zh@freelists.org">wine-zh@freelists.org</a></b><br>
    [<a href="http://www.freelists.org/list/wine-zh">In- uitschrijven</a>]
    [<a href="http://www.freelists.org/archive/wine-zh/">Archief</a>]<br>
    Een lijst met weinig berichten (10/dag) voor discussies in het Chinees.
  </li>

  <li>
    <b><a href="mailto:wine-devel@winehq.org">wine-devel@winehq.org</a></b><br>
    [<a href="//www.winehq.org/mailman/listinfo/wine-devel">In- uitschrijven</a>]
    [<a href="//www.winehq.org/pipermail/wine-devel/">Archief</a>]
    Een lijst met ongeveer 100 berichten per dag voor de discussie over de ontwikkeling van Wine,
    WineHQ, patches en alle andere interessante dingen voor Wine ontwikkelaars.
  </li>

  <li>
    <b><a href="mailto:wine-cvs@winehq.org">wine-cvs@winehq.org</a></b><br>
    [<a href="//www.winehq.org/mailman/listinfo/wine-cvs">In- uitschrijven</a>]
    [<a href="//www.winehq.org/pipermail/wine-cvs/">Archief</a>]
    Een alleen-lezen lijst met ongeveer 25 berichten per dag met de Git-commits.
  </li>

  <li>
    <b><a href="mailto:wine-releases@winehq.org">wine-releases@winehq.org</a></b><br>
    [<a href="https://www.winehq.org/mailman3/postorius/lists/wine-releases.winehq.org/">In- uitschrijven</a>]
    [<a href="https://www.winehq.org/mailman3/hyperkitty/list/wine-releases@winehq.org/">Archief</a>]<br>
    Een lijst (alleen-lezen) met ongeveer 2 berichten per maand met alle verschillen
    bij iedere officiële Wine versie.
  </li>

  <li>
    <b><a href="mailto:wine-bugs@winehq.org">wine-bugs@winehq.org</a></b><br>
    [<a href="//www.winehq.org/mailman/listinfo/wine-bugs">In- uitschrijven</a>]
    [<a href="//www.winehq.org/pipermail/wine-bugs/">Archief</a>]
    Een drukke lijst (100/dag) die alle activiteit op de
    <a href="//bugs.winehq.org/">Bug Tracking Database</a> laat zien.
  </li>

</ul>
