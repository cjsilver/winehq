<kc>
  <title>Wine Traffic</title>
  <author contact="mailto:wwn@winehq.org">Cressida Silver</author>
  <issue num="406" date="10/29/2021"/>
  <intro>
      <p>
        This is the 406th issue of the World Wine News publication. Its main goal is to inform you of what's going on around Wine.
        Wine is an open source implementation of the Windows API on top of X and Unix. Think of it as a Windows compatibility layer.
        Wine does not require Microsoft Windows, as it is a completely alternative implementation consisting of 100% Microsoft-free code,
        but it can optionally use native system DLLs if they are available. You can find more info at <a href="http://www.winehq.org">www.winehq.org</a>
      </p>
  </intro>

  <section title="New Author" subject="WWN" archive="" posts="">
    <p>
    Cressida Silver here. I am new to this, so please let's all play nice in the sandbox. I recently broke up with my former employer to start seeing CodeWeavers. So far so good. The goal is to bring you an edition of WWN the Friday following a Wine release. Feel free to provide tips, suggestions, insights, or generally troll me (if you must) <a href="mailto:wwn@winehq.org">here.</a>
    </p>

    <p>
    Great, now that is out of the way and we are all friends, let's get to it.
    </p>
  </section>

  <section title="Highlights From Wine 6.18 to Today" subject="Overview" archive="" posts="">
    <p>
      <b>Wine 6.18</b>
      <ul>
        <li>Shell32 and WineBus libraries converted to Portable Executable (PE)</li>
        <li>Unicode data updated to Unicode version 14</li>
        <li>Mono engine updated to version 6.4.0, with COM improvements</li>
        <li>More work towards Dwarf 3/4 debug support</li>
        <li>HID joystick enabled by default</li>
      </ul>
    </p>

    <p>
      <b>Wine 6.19</b>
      <ul>
        <li>IPHlpApi, NsiProxy, WineDbg and a few other modules converted to PE</li>
        <li>Additional work on HID</li>
        <li>Kernel parts of GDI moved to Win32u</li>
        <li>More work towards Dwarf 3/4 debug support</li>
      </ul>
    </p>

    <p>
      <b>Wine 6.20</b>
      <ul>
        <li>MSXml, XAudio, DInput and a few other modules converted to PE</li>
        <li>A few system libraries are bundled with the source to support PE builds</li>
        <li>HID joystick is now the only supported joystick backend in DirectInput</li>
        <li>Better support for MSVCRT builds in Winelib</li>
      </ul>
    </p>
  </section>

  <section title="Dinput Now Uses HID" subject="HID" archive="" posts="">
    <p>
      Tip of the hat to Rémi for this update. Previously, Wine’s DirectInput Implementation (dinput.dll and dinput8.dll) would talk directly to the hardware, and so would Wine's implementation of the lower-level HID APIs. Seeing double anyone?
      That's right, Wine was talking to the hardware in two locations. Less than ideal, as this resulted in redundant
      code.
    </p>

    <p>
      Windows currently implements dinput on top of the lower-level APIs. So Rémi’s work changed Wine to match that layout by
      placing dinput above the lower-level APIs instead of allowing dinput to talk directly to the hardware. This results in a Wine that more accurately reflects Windows behavior, and allows applications to talk to both HID and dinput, as well as connect information from one to the other.
    </p>

    <p>
      Fewer places for APIs to speak with the hardware means fewer lines of code. Fewer lines of code means fewer opportunities for bugs.
      Additionally, these changes will also make supporting additional backends easier in the future. The new layout means Wine more accurately reflects Windows behavior making controller support more consistent, and improving functionality of Windows applications that use both HID and dinput APIs.
    </p>
  </section>

  <section title="Unicode 14" subject="Unicode 14" archive="https://www.winehq.org/pipermail/wine-devel/2021-September/195389.html" posts="1">
    <p>
      Nikolay has moved Wine from the boring world of Unicode 13 to the brave new world of Unicode 14. Who wants to live in a world without the melting smiley face or the bitten lower lip? Not us — that’s for sure. When Unicode 14 is rolled out to the interwebs at large, there will be a grand total of 144,697 characters. Of course, Wine requires the underlying system fonts to support the new Unicode characters, which will take some time.
    </p>

    <p>
      One hundred forty-four thousand six hundred ninety-seven characters on the wall, one hundred forty-four thousand... this is gonna take a while. In the meantime, you can read more about Unicode 14 <a href="//unicode.org/versions/Unicode14.0.0/">here.</a>
    </p>
  </section>

  <section title="Wine Mono" subject="Wine Mono" archive="" posts="">
    <p>
      Updates to Wine Mono — the open-source, cross-platform implementation of .NET framework — by Esme improve the functionality
      of the following:
      <ul>
        <li>IL-2 Sturmovik</li>
        <li>Bizhawk</li>
        <li>Magicka</li>
        <li>FLY’N</li>
        <li>The Sims 3 (launcher)</li>
        <li>Sol Survivor</li>
        <li>Stride</li>
        <li>LiveSplit</li>
      </ul>
    </p>

    <p>
      Also not to be overlooked, Esme has updated FNA to 21.08 and SDL to 2.0.16. FNA now uses mfplat instead of libtheorafile for media file handling, making it possible to load .wmv and .wma files if Wine has the necessary support.
    </p>
  </section>
</kc>
