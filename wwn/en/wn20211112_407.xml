<kc>
  <title>Wine Traffic</title>
  <author contact="mailto:wwn@winehq.org">Cressida Silver</author>
  <issue num="407" date="11/12/2021"/>
  <intro>
      <p>
        This is the 407th issue of the World Wine News publication. Its main goal is to inform you of what's going on around Wine.
        Wine is an open source implementation of the Windows API on top of X and Unix. Think of it as a Windows compatibility layer.
        Wine does not require Microsoft Windows, as it is a completely alternative implementation consisting of 100% Microsoft-free code,
        but it can optionally use native system DLLs if they are available. You can find more info at <a href="http://www.winehq.org">www.winehq.org</a>
      </p>
  </intro>

  <section title="Musings" subject="" archive="" posts="">
    <p>
      Let me address the elephant... erm… not in the room. The AppDB/Bugzilla stats. That section was omitted in the last WWN issue. (A finely tuned eye will also notice it is missing from this issue.) There was much discussion on whether honor the tradition of including them, and – for now anyway – the consensus is to omit them. This may change in the future.
    </p>
    <p>
      As always, please feel free to let <a href="mailto:wwn@winehq.org">me</a>know what feels you get from what we are doing over here, good or bad.
    </p>
    <p>
      <b>This is a friendly reminder to all you lovely folks out there, Wine 7.0 is on the horizon, so be prepared for the code freeze sometime in December.</b>
    </p>
  </section>

  <section title="Wine Adds DWARF 3 and 4 Support" subject="" archive="" posts="">
    <p>
      Wine is working towards DWARF 4 being the default debug format. DWARF is a debugging file format used by many   compilers and debuggers to support source level debugging. Before we get too deep into this, let us discuss the name. According to the <a href="http://wiki.dwarfstd.org/index.php?title=DWARF_FAQ">DWARF Debugging Standard</a> FAQ, the name DWARF was intended as a pun to play off the acronym for the Executable and Linkable Format (ELF) it was developed alongside.
    </p>
    <p>
      Compilers in the Windows space use PDB/CodeView debug information. Compilers in the Linux space use DWARF debug information (e.g., from <a href="https://gcc.gnu.org/">GCC</a> and <a href="https://llvm.org/">LLVM</a>). Though LLVM/Clang does PDB/CodeView as well. Most debuggers in the Windows world, as well as Wine’s debugger (winedbg), rely on dbghelp.dll for access to the debugger information (e.g., symbols, types, or variables). So Wine’s dbghelp.dll implementation covers PDB/CodeView and DWARF while the native dbghelp.dll only overs PDB/CodeView.
    </p>
    <p>
      Linux adopted DWARF 2 in the late 1990s. For at least the last 15 years, Wine has been using DWARF 2. Meanwhile, the DWARF consortium has published three more specifications as follows:
        <ul>
          <li>DWARF 3 — 2005</li>
          <li>DWARF 4 — 2010</li>
          <li>DWARF 5 — 2017</li>
        </ul>
      </p>
      <p>
      Over the last six months to a year, GCC and most Linux distros have made DWARF 5 their default formats. Wine needs to catch up with the compilers out there, and support for DWARF 2 is likely to dwindle going forward. Thanks to Eric's hard work, Wine's dbghelp.dll now supports the most common DWARF 4 elements, and inline functions for both DWARF and PDB/CodeView. Not to mention that Wine Staging is using DWARF 4.
      </p>
      <p>
       Before DWARF 4 can be Wine's default debug format, <a href="https://www.winehq.org/pipermail/wine-devel/2021-November/199884.html">testing and feedback</a>is needed. Once work on DWARF 4 is finished, work on DWARF 5 can begin, and that is the goal... it is just going to take time.
      </p>
  </section>

  <section title="VKD3D-Proton v2.5 Released" subject="" archive="" posts="">
    <p>
      VKD3D-Proton is a gaming-focused implementation of Direct3D 12 developed for use with... you guessed it, Proton... and on October 18th (it may be old news, but it <b>is</b> news), v2.5 was released.
    </p>
    <p>
      It was a release with a little bit of everything:
      <ul>
        <li>Significant DXR work</li>
        <li>NVIDIA contributed integration APIs that enable DLSS support in D3D12 titles</li>
        <li>DXIL translation support to catch up with native drivers</li>
      </ul>
    </p>

    <p>
      It also includes fixes and workarounds to improve the following games:
        <ul>
          <li>DEATHLOOP</li>
          <li>F1 2021</li>
          <li>WRC 10</li>
          <li>DIRT 5</li>
          <li>Diablo II</li>
          <li>Psychonauts 2</li>
          <li>Far Cry 6</li>
          <li>Evil Genius 2: World Domination</li>
          <li>Hitman 3</li>
          <li>Anno 1800</li>
      </ul>
    </p>

    <p>
      Full VKD3D-Proton v2.5 release notes available <a href="https://github.com/HansKristian-Work/vkd3d-proton/releases/tag/v2.5">here.</a>
    </p>
    <p>
      For all you overachievers out there, if you are looking for some extra credit, take a gander at <a href="https://themaister.net/blog/2021/11/07/my-personal-hell-of-translating-dxil-to-spir-v-part-3/">this piece</a> written by Hans-Kristian Arntzen. Don't worry, I won't quiz you on it.
    </p>
  </section>
</kc>
