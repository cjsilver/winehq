<kc>
  <title>Wine Traffic</title>
  <author contact="mailto:wwn@winehq.org">Cressida Silver</author>
  <issue num="407" date="11/12/2021"/>
  <intro>
      <p>
        这是 World Wine News 第 407 期，为您带来 Wine 的最新动态。
        Wine 是基于 X 和 Unix 的 Windows API 的开源实现，可将其视为 Windows 的兼容层。
        Wine 的运行不需要依赖 Windows 系统，因为它是由 100% 无微软代码组成的、完全替代 Windows 的实现。
        当然，它也可以选择使用原生系统的 DLL（如果可用）。可以在 <a href="http://www.winehq.org">www.winehq.org</a> 这里找到更多信息。
      </p>
  </intro>

  <section title="Musings" subject="" archive="" posts="">
    <p>
      嗯…有一个不怎么明显的更新：AppDB/Bugzilla 的统计数据部分，在上一期中去掉了。（眼尖的人也能发现它在本期中也没有了。）关于是否要尊重传统保留它们进行了很多讨论，然后，目前为止一致的看法是去掉它们，虽然这在将来可能会有所改变。
    </p>
    <p>
      照惯例，如果对我们正在做的东西有什么想法，无论好的不好的，都请随时与<a href="mailto:wwn@winehq.org">我</a>联系。
    </p>
    <p>
      <b>另外友情提示，Wine 7.0 即将到来，大家要为 12 月份某个时间的代码冻结做好准备喔。</b>
    </p>
  </section>

  <section title="Wine Adds DWARF 3 and 4 Support" subject="" archive="" posts="">
    <p>
      Wine 正在努力使 DWARF 4 成为默认的调试格式。DWARF 是很多编译器和调试器用来支持源代码级调试的调试文件格式。在深入研究之前，让我们讨论一下它的命名。根据 <a href="http://wiki.dwarfstd.org/index.php?title=DWARF_FAQ">DWARF 调试标准</a> 的常问问题，DWARF（小矮人）这个名字是作为一个双关语来演绎与它一起开发的 Executable and Linkable Format 首字母缩写词 - ELF（精灵）。
    </p>
    <p>
      Windows 空间中的编译器使用 PDB/CodeView 调试信息。Linux 空间中的编译器使用 DWARF 调试信息（比如：从 <a href="https://gcc.gnu.org/">GCC</a> 到 <a href="https://llvm.org/">LLVM</a>）。尽管 LLVM/Clang 也支持 PDB/CodeView，大多数 Windows 调试器，以及 Wine 的调试器（winedbg）都依赖 dbghelp.dll 来访问调试信息（比如：符号，类型或变量）。所以 Wine 的 dbghelp.dll 实现覆盖了 PDB/CodeView 和 DWARF，而原生的 dbghelp.dll 只支持 PDB/CodeView。
    </p>
    <p>
      Linux 在 1990 年代后期才用了 DWARF 2。至少在之后的 15 年里，Wine 一直使用着 DWARF 2。同时，DWARF 联盟发布了另外 3 个版本，如下：
        <ul>
          <li>DWARF 3 — 2005</li>
          <li>DWARF 4 — 2010</li>
          <li>DWARF 5 — 2017</li>
        </ul>
      </p>
      <p>
        过去半年到一年的时间里，GCC 和大多数的 Linux 发行版都使用了 DWARF 5 作为它们默认的格式，Wine 需要跟随这些编译器，未来可能会减少对 DWARF 2 的支持。感谢 Eric 的辛勤工作，Wine 的 dbghelp.dll 现在支持大多数常见的 DWARF 4 元素，以及 DWARF 和 PDB/CodeView 的内联函数。更不用说 Wine Staging 正在使用 DWARF 4。
      </p>
      <p>
       在 DWARF 4 成为 Wine 的默认调试格式之前，<a href="https://www.winehq.org/pipermail/wine-devel/2021-November/199884.html">测试和反馈</a>是必需的。一旦完成了 DWARF 4 的工作，就可以开始 DWARF 5 的工作，这就是目标…只是需要时间。
      </p>
  </section>

  <section title="VKD3D-Proton v2.5 Released" subject="" archive="" posts="">
    <p>
      VKD3D-Proton 是 Direct3D 12 以游戏为中心的实现，专为与 Proton 一起使用而开发。并且在 10 月 18 日（这可能是旧新闻，但它<b>现在是</b>新闻），v2.5 发布了。
    </p>
    <p>
      该版本包含以下几方面的更新：
      <ul>
        <li>DirectX 光线追踪支持</li>
        <li>由 NVIDIA 贡献的，在 D3D12 中启用 DLSS 支持的集成 API</li>
        <li>DXIL 跟随原生驱动的翻译支持</li>
      </ul>
    </p>

    <p>
      它还包括改进以下游戏的修复和临时解决方案：
        <ul>
          <li>死亡循环</li>
          <li>F1 2021</li>
          <li>世界汽车拉力锦标赛 10</li>
          <li>尘埃 5</li>
          <li>暗黑破坏神 II</li>
          <li>意航员 2</li>
          <li>孤岛惊魂 6</li>
          <li>邪恶天才 2：世界统治</li>
          <li>杀手 3</li>
          <li>纪元 1800</li>
      </ul>
    </p>

    <p>
      完整的 VKD3D-Proton v2.5 发布说明可在 <a href="https://github.com/HansKristian-Work/vkd3d-proton/releases/tag/v2.5">这里</a> 获得。
    </p>
    <p>
      成绩优异想要获得附加分的人，可以看看 Hans-Kristian Arntzen 写的 <a href="https://themaister.net/blog/2021/11/07/my-personal-hell-of-translating-dxil-to-spir-v-part-3/">这篇文章</a>。别担心，我不会考你。
    </p>
  </section>
</kc>
